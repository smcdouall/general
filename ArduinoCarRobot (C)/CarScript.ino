
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <NewPing.h>

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61); 

// Select which 'port' M1, M2, M3 or M4. In this case, M1
Adafruit_DCMotor *rightMotor = AFMS.getMotor(4);
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
// You can also make another motor on port M2
//Adafruit_DCMotor *myOtherMotor = AFMS.getMotor(2);


int rLED = 4;
int yLED = 2;
int gLED = 3;

int analogPin = 3;



/*-----( Declare Constants and Pin Numbers )-----*/
#define  TRIGGER_PIN  11
#define  ECHO_PIN     10
#define MAX_DISTANCE 500 // Maximum distance we want to ping for (in centimeters).
                         //Maximum sensor distance is rated at 400-500cm.
#define  TRIGGER_PIN2  9
#define  ECHO_PIN2     8    

#define  TRIGGER_PIN3  7
#define  ECHO_PIN3     6   


                         
                         
/*-----( Declare objects )-----*/
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
NewPing sonar2(TRIGGER_PIN2, ECHO_PIN2, MAX_DISTANCE);
NewPing sonar3(TRIGGER_PIN3, ECHO_PIN3, MAX_DISTANCE);
/*-----( Declare Variables )-----*/
int DistanceIn;
int DistanceCm;

int distf;
int distr;
int distl;

int t;


void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  pinMode(rLED, OUTPUT);
  pinMode(yLED, OUTPUT);
  pinMode(gLED, OUTPUT);
  Serial.println("Adafruit Motorshield v2 - DC Motor test!");

  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz
  
  // Set the speed to start, from 0 (off) to 255 (max speed)
  leftMotor->setSpeed(255);
  rightMotor->setSpeed(255);
  delay(5000);
  Serial.println("Begin");

}


void Forward(){
  rightMotor->run(FORWARD);
  leftMotor->run(FORWARD);
}

void Backward(){
  rightMotor->run(BACKWARD);
  leftMotor->run(BACKWARD);
}

void Stop(){
  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
}

void Clockwise(){
  rightMotor->run(BACKWARD);
  leftMotor->run(FORWARD);
}

void Anticlockwise(){
  rightMotor->run(FORWARD);
  leftMotor->run(BACKWARD);
}

void RedOn(){
  digitalWrite(rLED,HIGH);
}

void YellowOn(){
  digitalWrite(yLED,HIGH);
}

void GreenOn(){
  digitalWrite(gLED,HIGH);
}


void RedOff(){
  digitalWrite(rLED,LOW);
}

void YellowOff(){
  digitalWrite(yLED,LOW);
}

void GreenOff(){
  digitalWrite(gLED,LOW);
}

int FrontSensor(){
  DistanceCm = sonar.ping_cm();
  Serial.print("Front Ping: ");
  Serial.print(DistanceCm); 
  Serial.println(" cm");
  delay(100);// Wait 100ms between pings (about 10 pings/sec)
  return DistanceCm;
}

int LeftSensor(){
  DistanceCm = sonar2.ping_cm();
  Serial.print("Left Ping: ");
  Serial.print(DistanceCm); 
  Serial.println(" cm");
  delay(100);// Wait 100ms between pings (about 10 pings/sec)
  return DistanceCm;
}

int RightSensor(){
  DistanceCm = sonar3.ping_cm();
  Serial.print("Right Ping: ");
  Serial.print(DistanceCm); 
  Serial.println(" cm");
  delay(100);// Wait 100ms between pings (about 10 pings/sec)
  return DistanceCm;
}

void loop() {
  
  //while(
//  int val = analogRead(analogPin);    // read the input pin
//  while (val 
//  Serial.println(val);
//  delay(100);

//while(1){
  //Clockwise();
//}
  
  t = 0;
  Forward();
  GreenOn();
  YellowOff();
  RedOff();
  
  
  distf = FrontSensor();
  
  if (distf <= 30){
    delay(100);
    Serial.println("checking value again");
    distf = FrontSensor();
    if (distf <= 30){
      Stop();
      GreenOff();
      YellowOff();
      RedOn();
      Serial.println("stopping");
      delay(2000);
      //GreenOff();
      //RedOn();
      //delay(2000);
      
      while (t == 0){
        distr = RightSensor();
        distl = LeftSensor();
         
        if (distr > 50){
          Clockwise();
          //GreenOff();
          RedOff();
          YellowOn();
          GreenOff();
          delay(1000);
          distf = FrontSensor();
          
          while (distf <= 50){
            distf = FrontSensor();
            Serial.println("turning right");
          }
          Stop();
          RedOn();
          YellowOff();
          GreenOff();
          Serial.println("stopping");
          delay(2000);
          Serial.println("done turning right");
          t = 1;
        }
        
        else if (distl > 50){
          Anticlockwise();
          GreenOff();
          RedOff();
          YellowOn();
          delay(1000);
          distf = FrontSensor();
          
          while (distf <= 50){
            distf = FrontSensor();
            Serial.println("turning left");
          }
          Stop();
          RedOn();
          YellowOff();
          GreenOff();
          Serial.println("stopping");
          delay(2000);
          Serial.println("done turning right");
          t = 1;
        }
        
        else {
          
          Backward();
          Serial.println("backing up");
          distr = RightSensor();
          distl = LeftSensor();
          GreenOff();
          YellowOn();
          RedOff(); 
          
          while (distr <= 50 && distl <= 50){
            distr = RightSensor();
            distl = LeftSensor();
          } 
          Stop();
          RedOn();
          YellowOff();
          GreenOff();
          Serial.println("stopping");
          delay(2000);
          Serial.println("done backing up");
        
        }
        
      
        
      }
    }

  }
  
    
  else{
    Serial.println("continue moving forwards");
  }
  

}
