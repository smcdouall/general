import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {
	

	public static ArrayList<Entity> sortEntities(ArrayList<Entity> entities){
		//ArrayList<Entity> list = new ArrayList<Entity>();
		boolean done = false;
		int counter = 0;
		while (done == false){
			counter = 0;
			for (int i = 0; i < entities.size() - 1; i++){
				// take init value of first Entity and compare with second
				// continue this as via Bubble Sort
				
				// If value is smaller swap it
				if (entities.get(i).getInit() < entities.get(i+1).getInit()){
					Collections.swap(entities, i, i+1);
				}
				// If equal, see if player or not, player gets priority
				else if  (entities.get(i).getInit() == entities.get(i+1).getInit()){
					if (entities.get(i).getPlayer() == 0 && entities.get(i+1).getPlayer() == 1){
						Collections.swap(entities, i, i+1);
					}
					else{
						counter++;
					}
				
				}
				// If value is larger, leave it and move on
				else {
					counter++;
				}
				

				
			}
			if (counter == entities.size()-1){
				done = true;
			}
			

		}
		
		
		
		
		return entities;
	}
	
	
	

	public static void main(String[] args) {
		
//		ArrayList<Entity> list = new ArrayList<Entity>();
//		ArrayList<Entity> newlist = new ArrayList<Entity>();
//		Entity r = new Entity("r", 6 ,0);
//		Entity a = new Entity("a", 5 ,0);
//		Entity b = new Entity("b", 15 ,0);
//		Entity q = new Entity("q", 5 ,1);
//		list.add(r);
//		list.add(a);
//		list.add(b);
//		list.add(q);
//		newlist = sortEntities(list);
//		for (int i = 0; i < newlist.size(); i++){
//			System.out.println(newlist.get(i).getName());
//		}
		
		
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("How many players in this combat?");
		int playernumber = scanner.nextInt();
		ArrayList<Entity> unsorted = new ArrayList<Entity>();
		ArrayList<Entity> sorted = new ArrayList<Entity>();
		
		while (playernumber != 0){
//			//  prompt for the user's name
		    System.out.print("Player Name: ");
//
//		    // get their input as a String
		    String username = scanner.next();
		    int turn = 1;
//
//		    // prompt for their init
		    System.out.print("Initiative: ");
//
//		    // get init
		    int init = scanner.nextInt();
//		    
		    username = username + " " + turn;
		    Entity q = new Entity(username, init ,1);
		    unsorted.add(q);
//		    //System.out.println(init);
//		    
//		    //reduce init by ten and if >0 add to hashmap
		    init = init - 10;
		    turn++;
		    username = username.replaceAll("\\d","");
		    username = username.trim();
		    username = username + " " + turn;
//		    
//		    
//		    
		    while(init>0){
			    Entity b = new Entity(username, init ,1);
			    unsorted.add(b);
//		    	//System.out.println(init);
		    	init = init - 10;
		    	turn++;
		    	username = username.replaceAll("\\d","");
		    	username = username.trim();
		    	username = username + " " + turn;
//		    	
		    }
//
//		    
		    playernumber--;
//			
		}
		
		System.out.println("How many enemies/NPCs in this combat?");
		int enemynumber = scanner.nextInt();
		
		while (enemynumber != 0){
//			//  prompt for the user's name
		    System.out.print("Enemy/NPC Name: ");
//
//		    // get their input as a String
		    String username = scanner.next();
		    int turn = 1;
//
//		    // prompt for their init
		    System.out.print("Initiative: ");
//
//		    // get init
		    int init = scanner.nextInt();
//		    
		    username = username + " " + turn;
		    Entity q = new Entity(username, init ,0);
		    unsorted.add(q);
//		    //System.out.println(init);
//		    
//		    //reduce init by ten and if >0 add to hashmap
		    init = init - 10;
		    turn++;
		    username = username.replaceAll("\\d","");
		    username = username.trim();
		    username = username + " " + turn;
//		    
//		    
//		    
		    while(init>0){
			    Entity b = new Entity(username, init ,1);
			    unsorted.add(b);
//		    	//System.out.println(init);
		    	init = init - 10;
		    	turn++;
		    	username = username.replaceAll("\\d","");
		    	username = username.trim();
		    	username = username + " " + turn;
//		    	
		    }
//
//		    
		    enemynumber--;
//			
		}
		
		// now sort them
		sorted = sortEntities(unsorted);
		
		for (int i = 0; i < sorted.size(); i++) {
			System.out.println(sorted.get(i).getName() + ": " + sorted.get(i).getInit());
		}

	}

}