


public class Entity {
	private String name;
	private int init;
	private int player;// 1 = player, 0 = NPC
	
	Entity(String Name, int Init,  int Player){
		name = Name;
		init = Init;
		player = Player;
	}
					
	public static void main(String[] args) {
		

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getInit() {
		return init;
	}

	public void setInit(int init) {
		this.init = init;
	}

	public int getPlayer() {
		return player;
	}

	public void setPlayer(int player) {
		this.player = player;
	}

	
	
}
