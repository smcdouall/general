﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour {

    public Sprite dmgSprite;
    public int hp = 4;
    public AudioClip chopSound1;
    public AudioClip chopSound2;

    public AudioClip superChopSound1;
    public AudioClip superChopSound2;

    private SpriteRenderer spriteRenderer;

    void Awake ()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void setFalse()
    {
        gameObject.SetActive(false);
    }

    public void DamageWall (int loss)
    {
        if (loss == 1)  // regular attack
        {
            SoundManager.instance.RandomizeSfx(chopSound1, chopSound2);
        }
        else // potion boosted attack
        {
            SoundManager.instance.RandomizeSfx(superChopSound1, superChopSound2);
        }

        spriteRenderer.sprite = dmgSprite;
        hp -= loss;
        if (hp <= 0)
        {

            Invoke("setFalse", 0.1f);
        }
    }

}
