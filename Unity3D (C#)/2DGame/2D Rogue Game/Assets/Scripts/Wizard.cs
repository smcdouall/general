﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Wizard : MonoBehaviour {

    public AudioClip wizardAttack1;
    public AudioClip wizardAttack2;


    // change colour each time object is created
    void Start()
    {

        gameObject.GetComponent<Renderer>().material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 1);


    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown("space") && NextToPlayer())
        {
            WizardAttack();
            print("wizard attack!");
        }
    }

    public bool NextToPlayer()  {

        Transform target;
        target = GameObject.FindGameObjectWithTag("Player").transform;
        print("X: " + Mathf.Abs(target.position.x - transform.position.x));
        print("Y: " + Mathf.Abs(target.position.y - transform.position.y));
        if (Mathf.Abs(target.position.x - transform.position.x) <= float.Epsilon + 1 && Mathf.Abs(target.position.y - transform.position.y) <= float.Epsilon + 1)    // check if difference between x coord target position and transform position is less than eps (close to 0) ie enemy and player are in same column
        {
            return true;
        }
        else
        {
            return false;
        }


        return true;
            
    }

    void WizardAttack ()
    {
        GameObject thePlayer = GameObject.Find("Player");   // do at gameover, therefore only need to call once
        Player player = thePlayer.GetComponent<Player>(); // Wizard takes food
        player.WizardTakesFood(30);

        GameObject[] gos = GameObject.FindGameObjectsWithTag("Enemy");



        for (var i = 0; i < gos.Length; i++)
        {
            if (gos[i] != null)
            {
                Destroy(gos[i]);
            }
        }

        GameManager.instance.deadEnemies = true;

        SoundManager.instance.RandomizeSfx(wizardAttack1, wizardAttack2);
        Destroy(gameObject); // Wizard goes
    }
}
