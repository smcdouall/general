﻿using System.Collections;
using System.Collections.Generic;   // so can use lists
using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour {


    [Serializable]
    public class Count
    {
        public int minimum;
        public int maximum;

        public Count(int min, int max) // can set min and max values when declare a new count
        {
            minimum = min;
            maximum = max;
        }
    }

    public int columns = 8; // 8x8 gameboard
    public int rows = 8;
    public Count wallCount = new Count(5, 9);   // range of walls going to be spawned, so 5-9 walls
    public Count foodCount = new Count(1, 5);   // same for food between 1-5
    //public Count potionCount = new Count(1, 3); // want a potion to sometimes appear
    public GameObject exit;
    public GameObject[] floorTiles;
    public GameObject[] wallTiles;
    public GameObject[] foodTiles;
    public GameObject[] enemyTiles;
    public GameObject[] outerWallTiles;
    public GameObject[] potionTiles;
    public GameObject[] wizardTiles;

    private Transform boardHolder; // going to be spawning lots of game objects and going to child them all to boardHolder
    private List<Vector3> gridPositions = new List<Vector3>();

    void InitialiseList()
    {
        gridPositions.Clear();  

        for (int x = 1;  x < columns - 1; x++)
        {
            for (int y = 1; y < rows - 1; y++)
            {
                gridPositions.Add(new Vector3(x, y, 0f));   // creating list of possible positions to place walls, enemies or pickups
            }
        }
    }

    void BoardSetup()   // lay out our outer wall tiles and background of floor tiles
    {
        boardHolder = new GameObject("Board").transform;

        for (int x = -1; x < columns + 1; x++)
        {
            for (int y = -1; y < rows + 1; y++)
            {
                GameObject toInstantiate = floorTiles[Random.Range(0, floorTiles.Length)];// choose floor tile at random and prepare to instantiate it
                if (x == -1 || x == columns || y == -1 || y == rows)    // is an outer wall?
                {
                    toInstantiate = outerWallTiles[Random.Range(0, outerWallTiles.Length)];
                }
                GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;

                instance.transform.SetParent(boardHolder);
            }
        }
    }

    Vector3 RandomPosition()
    {
        int randomIndex = Random.Range(0,gridPositions.Count);
        Vector3 randomPosition = gridPositions[randomIndex];
        gridPositions.RemoveAt(randomIndex);
        return randomPosition;
    }

    void LayoutObjectAtRandom(GameObject[] tileArray, int minimum, int maximum)
    {
        int objectCount = Random.Range(minimum, maximum + 1);

        for (int i = 0; i < objectCount; i++)
        {
            Vector3 randomPosition = RandomPosition();
            GameObject tileChoice = tileArray[Random.Range(0, tileArray.Length)];
            Instantiate(tileChoice, randomPosition, Quaternion.identity);
        }
    }

    bool RandomChance(float chance)
    {
        float roll = Random.Range(0.0f, 1.0f);
        print(roll);
        if (roll <= chance)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public void SetupScene (int level)
    {
        BoardSetup();
        InitialiseList();
        LayoutObjectAtRandom(wallTiles, wallCount.minimum, wallCount.maximum);
        LayoutObjectAtRandom(foodTiles, foodCount.minimum, foodCount.maximum);
        int enemyCount = (int)Mathf.Log(level, 2f);
        LayoutObjectAtRandom(enemyTiles, enemyCount, enemyCount);   // same as not specifying random range

        // Potions and Wizards will only spawn in 25 % of the time
        int wizardNumber = 0;
        int potionNumber = 0;
        if (RandomChance(0.25f))
        {
            wizardNumber = 1;
        }
        if (RandomChance(0.25f))
        {
            potionNumber = 1;
        }
        LayoutObjectAtRandom(wizardTiles, wizardNumber, wizardNumber);
        LayoutObjectAtRandom(potionTiles, potionNumber, potionNumber);
        Instantiate(exit, new Vector3(columns - 1, rows - 1, 0F), Quaternion.identity);

    }
}
