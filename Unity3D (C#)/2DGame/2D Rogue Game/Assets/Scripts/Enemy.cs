﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MovingObject {

    public static Enemy instance = null;  // can now access public variables and fucntions of game manager from any script in game

    public int playerDamage;


    private Animator animator;
    private Transform target;
    private bool skipMove;  // cause enemy to move every other turn
    public AudioClip enemyAttack1;
    public AudioClip enemyAttack2;

    public bool dead = false; // if wizard has attacked, enemy is dead

	protected override void Start ()
    {
        GameManager.instance.AddEnemyToList(this);  // game manager can now call public function in Enemy.cs
        animator = GetComponent<Animator>();
        target = GameObject.FindGameObjectWithTag ("Player").transform;
        base.Start();	
	}
	


    protected override void AttemptMove <T> (int xDir, int yDir)
    {

        if (skipMove)
        {
            skipMove = false;
            return;
        }
        base.AttemptMove<T>(xDir, yDir);
        skipMove = true;

    }


    public void MoveEnemy() // enemy strategy / AI
    {
        int xDir = 0;
        int yDir = 0;

        if (Mathf.Abs (target.position.x - transform.position.x) <float.Epsilon)    // check if difference between x coord target position and transform position is less than eps (close to 0) ie enemy and player are in same column
        {
            yDir = target.position.y > transform.position.y ? 1 : -1; // true = +1 , false = -1
        }
        else
        {
            xDir = target.position.x > transform.position.x ? 1 : -1;
        }
        AttemptMove<Player>(xDir, yDir);
    }

    
    protected override void OnCantMove <T> (T component)
    {
        Player hitPlayer = component as Player;

        animator.SetTrigger("enemyAttack"); 

        hitPlayer.LoseFood(playerDamage);

        SoundManager.instance.RandomizeSfx(enemyAttack1, enemyAttack2);
    }

    public void DestroyEnemy()
    {
        Destroy(gameObject); // destroyed when wizard attacks
    }
}
