﻿using System.Collections;
using System.Collections.Generic;   // so can use lists
using System;
using UnityEngine;
using Random = UnityEngine.Random;

public abstract class MovingObject : MonoBehaviour {    // abstract = create classes and class members that are incomplete and must be implemented in the derieved class

    public float moveTime = 0.1f;   // time will take object to move (seconds)
    public LayerMask blockingLayer; // layer going to check collisions on to determine if space is open to be moved into


    private BoxCollider2D boxCollider;
    private Rigidbody2D rb2D;
    private float inverseMoveTime; // make movement calculations more efficient 

	// Use this for initialization
	protected virtual void Start () {
        boxCollider = GetComponent<BoxCollider2D>();
        rb2D = GetComponent<Rigidbody2D>();
        inverseMoveTime = 1f / moveTime;    // so can multiply instead of divide, more efficient
	}

    protected bool Move (int xDir, int yDir, out RaycastHit2D hit)
    {
        Vector2 start = transform.position;
        Vector2 end = start + new Vector2(xDir, yDir);

        boxCollider.enabled = false; // don't want to hit own collider
        hit = Physics2D.Linecast(start, end, blockingLayer);
        boxCollider.enabled = true;

        if (hit.transform == null)
        {
            StartCoroutine(SmoothMovement(end));
            return true;
        }

        return false;
    }

    protected IEnumerator SmoothMovement (Vector3 end)  // end = where to move to
    {
         // sqr magnitude is more efficient than magnitude
        float sqrRemainingDistance = (transform.position - end).sqrMagnitude;
        while (sqrRemainingDistance > float.Epsilon)
        {
            Vector3 newPostion = Vector3.MoveTowards(rb2D.position, end, inverseMoveTime * Time.deltaTime);
            rb2D.MovePosition(newPostion); // use this to move to new position just found
            sqrRemainingDistance = (transform.position - end).sqrMagnitude; // recalculate remaining distance after moved
            yield return null;
        }
    }

    protected virtual void AttemptMove <T> (int xDir, int yDir)
            where T : Component
    {
        RaycastHit2D hit;
        bool canMove = Move(xDir, yDir, out hit);   // if nothing hit, going to return and not execute following code

        if (hit.transform == null)
        {
            return;
        }

        T hitComponent = hit.transform.GetComponent<T>();

        if (!canMove && hitComponent != null)
        {
            OnCantMove(hitComponent);
        }
    }
    protected abstract void OnCantMove<T>(T component)  //overriden by functions in the inheriting class
            where T : Component;
}
