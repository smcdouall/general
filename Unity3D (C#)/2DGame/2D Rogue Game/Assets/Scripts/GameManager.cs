﻿using System.Collections;
using System.Collections.Generic;   // using lists to keep track of enemies
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class GameManager : MonoBehaviour {


    // Use this for initialization

    public float levelStartDelay = 2f;
    public float turnDelay = .1f; 
    public static GameManager instance = null;  // can now access public variables and fucntions of game manager from any script in game
    public BoardManager boardScript;
    public int playerFoodPoints = 100;
    public int playerTotal = 0;
    [HideInInspector] public bool playersTurn = true; // public but not displayed in editor

    public bool deadEnemies = false;

    private Text levelText;
    private GameObject levelImage;
    private int level = 0;
    public List<Enemy> enemies;
    private bool enemiesMoving;
    private bool doingSetup;


    private int highestFoodScore = 100;
    

	public void Awake ()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this) 
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject); // want to use game manager to keep track of score between scenes
        boardScript = GetComponent<BoardManager>();
        //InitGame();	
	}

    void InitGame()
    {
        doingSetup = true;  // player can't move while title card is up
        deadEnemies = false;    // otherwise won't act if wizard attacked in previous levels
        levelImage = GameObject.Find("LevelImage");
        levelText = GameObject.Find("LevelText").GetComponent<Text>();
        levelText.text = "Day " + level;
        levelImage.SetActive(true);
        Invoke("HideLevelImage", levelStartDelay);
        enemies.Clear();
        boardScript.SetupScene(level);  // pass in level, so can tell board script level to determine number of enemies
    }

    private void HideLevelImage()
    {
        levelImage.SetActive(false);
        doingSetup = false; // player can now move
    }


    public void updateHighest(int currentScore) // checking if new food score is highest that has been achieved so far in the game
    {

        if (currentScore > highestFoodScore)
        {
            highestFoodScore = currentScore;
        }

    }

    public void GameOver()
    {

        GameObject thePlayer = GameObject.Find("Player");   // do at gameover, therefore only need to call once
        Player player = thePlayer.GetComponent<Player>();



        // menu reseting now fixed, needed to destroy the old GameManager and SoundManager to allow new one to be created
        Destroy(gameObject); // GM
        



        levelText.text = "After " + level + " days, you starved.\n\n" + "Your high score this life was " + highestFoodScore + " food \n\n" + "You managed to gather a total of " + player.totalFoodGathered + " food";
        
        levelImage.SetActive(true);
        enabled = false;

    }



    // Update is called once per frame
    void Update () {

        GameObject thePlayer = GameObject.Find("Player");   // obatining the food variable from player and using it to test whether we have reached a new highest score
        Player player = thePlayer.GetComponent<Player>();
        updateHighest(player.food);


        if (playersTurn || enemiesMoving || doingSetup)
        {
            return;
        }

        StartCoroutine(MoveEnemies());


        
	}

    public void AddEnemyToList (Enemy script)
    {
        enemies.Add(script);
    }

    IEnumerator MoveEnemies()
    {

        enemiesMoving = true;

        yield return new WaitForSeconds(turnDelay);
            if (enemies.Count == 0 || deadEnemies)
            {

            yield return new WaitForSeconds(turnDelay);
            }
        if (!deadEnemies)                                   // don't want to enter this if wizard attacks
        {
            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].MoveEnemy();
                yield return new WaitForSeconds(enemies[i].moveTime);
            }
        }

        playersTurn = true;



            enemiesMoving = false;

        //else
        //{
        //    yield return new WaitForSeconds(turnDelay);         /// SDJFKSLJFKLASJFKLSDJKFS DHERES IS THE OPRPOBLEMS 
        //    print("enemies are dead!");
        //    enemiesMoving = false;
        //    playersTurn = true;
        //}

  
    }

    //This is called each time a scene is loaded.
    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        //Add one to our level number.
        level++;
        //Call InitGame to initialize our level.
        InitGame();
    }
    void OnEnable()
    {
        //Tell our ‘OnLevelFinishedLoading’ function to
       // start listening for a scene change event as soon as
        //this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }
    void OnDisable()
    {

    SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }
}
