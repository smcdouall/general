﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MovingObject {

    public static Player instance = null;  // can now access public variables and fucntions of game manager from any script in game

    public int wallDamage = 1;  // amount of damage player inflicts to walls
    public int powerUpDuration = 0;
    public int pointsPerFood = 10;
    public int pointsPerSoda = 20;
    public float restartLevelDelay = 1f;
    public Text foodText;

    public AudioClip moveSound1;
    public AudioClip moveSound2;
    public AudioClip eatSound1;
    public AudioClip eatSound2;
    public AudioClip drinkSound1;
    public AudioClip drinkSound2;
    public AudioClip gameOverSound;
    public AudioClip gameOverMusic1;
    public AudioClip gameOverMusic2;

    private Animator animator;
    public int food;

    public int totalFoodGathered;   // total food gathered excluding loses, just some more stats for the player to know on death

    // Use this for initialization
    protected override void Start ()
    {  // different implementation in player class from moving object class
        animator = GetComponent<Animator>();

        food = GameManager.instance.playerFoodPoints;   // starting food
        totalFoodGathered = GameManager.instance.playerTotal;

        foodText.text = "Food: " + food;    //update text

        base.Start();	
	}

    private void OnDisable()
    {
        GameManager.instance.playerFoodPoints = food;   // used to store food over levels
        GameManager.instance.playerTotal = totalFoodGathered;   // + total gathered food
    }
	
	// Update is called once per frame
	void Update ()
    {
	    if (!GameManager.instance.playersTurn)
        {
            return;
        }

        int horizontal = 0;
        int vertical = 0;

        horizontal = (int)Input.GetAxisRaw("Horizontal");
        vertical = (int)Input.GetAxisRaw("Vertical");

        if (horizontal != 0)
        {
            vertical = 0;
        }

        if (horizontal != 0 || vertical != 0)
        {
            AttemptMove<Wall>(horizontal, vertical);    // player may encounter wall
        }
    }

    protected override void OnCantMove <T> (T component)
    {
        Wall hitWall = component as Wall; // casting component to wall
        hitWall.DamageWall(wallDamage);
        animator.SetTrigger("playerChop");
        if (powerUpDuration > 0)
        {
            powerUpDuration--;
        }
        else
        {
            wallDamage = 1;
        }
    }

    private void Restart()
    {
        Application.LoadLevel(Application.loadedLevel);

    }

    public void LoseFood (int loss)
    {
        animator.SetTrigger("playerHit");
        food -= loss;
        foodText.text = "-" + loss + " Food: " + food;
        CheckIfGameOver();
    }

    public void WizardTakesFood(int loss)
    {
        animator.SetTrigger("playerChop");
        food -= loss;
        foodText.text = "-" + loss + " Food: " + food;
        CheckIfGameOver();
    }


    protected override void AttemptMove <T> (int xDir, int yDir)
    {
        food--;
        foodText.text = "Food: " + food;
        base.AttemptMove<T>(xDir, yDir);
        RaycastHit2D hit;

        if (Move (xDir, yDir, out hit))
        {
            SoundManager.instance.RandomizeSfx(moveSound1, moveSound2);
        }

        CheckIfGameOver();
        GameManager.instance.playersTurn = false;
    }

    private void OnTriggerEnter2D (Collider2D other)
    {
        if (other.tag == "Exit")
        {
            Invoke("Restart", restartLevelDelay);
            enabled = false;
            
        }
        else if (other.tag == "Food")
        {
            food += pointsPerFood;
            totalFoodGathered += pointsPerFood;
            //print(totalFoodGathered);
            foodText.text = "+" + pointsPerFood + " Food: " + food;
            SoundManager.instance.RandomizeSfx(eatSound1, eatSound2);
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "Soda")
        {
            food += pointsPerSoda;
            totalFoodGathered += pointsPerSoda;
            //print(totalFoodGathered);
            foodText.text = "+" + pointsPerSoda + " Food: " + food;
            SoundManager.instance.RandomizeSfx(drinkSound1, drinkSound2);
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "Potion") // gain a power up and can temporarily instantly destroy walls!
        {
            foodText.text = "You gained a power up!";
            powerUpDuration = 4;    // five walls
            wallDamage = 4;
            SoundManager.instance.RandomizeSfx(drinkSound1, drinkSound2);
            other.gameObject.SetActive(false);
        }
    }

    private void CheckIfGameOver()
    {
        if (food <= 0)
        {
            //foodText.text = "Food: 0";
            SoundManager.instance.PlaySingle(gameOverSound);
            SoundManager.instance.musicSource.Stop();
            Invoke("GameOverMusic", 0.5f);
            GameManager.instance.GameOver();
            Invoke("ToMenu", 10.0f); // after 10 seconds take back to main menu

        }
    }

    public void GameOverMusic()
    {
        SoundManager.instance.RandomizeSfx(gameOverMusic1,gameOverMusic2);
    }

    public void ToMenu()
    {
        SoundManager.instance.GameOver();   // use this to get rid of the SM, like we did with the GM when reseting the level
        SceneManager.LoadScene("Menu");
    }

    public IEnumerator Wait(float delayInSecs)
    {
        yield return new WaitForSeconds(delayInSecs);
    }
}
