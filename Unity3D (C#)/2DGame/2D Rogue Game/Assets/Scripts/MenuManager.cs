﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {


    public void ToGame()
    {

        //GameManager someName = new GameManager();
        //someName.Awake();
        SceneManager.LoadScene("Main"); // loads the main game
        
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
